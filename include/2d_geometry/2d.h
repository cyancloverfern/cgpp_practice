#pragma once

template<typename T>
struct point2d {
  T x, y;
};

template<typename T>
struct vector2d : public point2d<T> {
  
};

template<typename T>
struct covector2d : public point2d<T> {
  
};

template<typename T>
T operator*(covector2d<T> const& lhs, vector2d<T> const& rhs)
{
  return lhs.x * rhs.x + lhs.y * rhs.y;
}
