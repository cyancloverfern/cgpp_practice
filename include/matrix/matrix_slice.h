#pragma once

#include <array>
#include <numeric>
#include <concepts>
#include <initializer_list>
#include <cassert>

using std::size_t;

template<size_t N>
struct Matrix_slice {
  public:
    Matrix_slice() = default;

    Matrix_slice(size_t s, std::initializer_list<size_t> exts)
      : start(s), extents(exts)
    { }

    Matrix_slice(size_t s, std::initializer_list<size_t> exts, std::initializer_list<size_t> strs)
      : start(s), extents(exts), strides(strs)
    { }

    template<typename... Dims>
    Matrix_slice(Dims... dims)
      : size(1), start(0)
    {
      static_assert(sizeof... (Dims) == N, "");
      size_t args[N] { size_t(dims)... };
      for (size_t i = 0; i != N; ++i) {
        strides[(N - 1) - i] = size;
        extents[i] = args[i];
        size *= args[i];
      }
    }

    template<typename... Dims>
      requires  (std::is_convertible_v<Dims, int> && ...)
    size_t operator()(Dims... dims) const
    {
      static_assert(sizeof... (Dims) == N, "");
      size_t args[N] { size_t(dims)... };
      return std::inner_product(args, args + N, strides.begin(), size_t(0));
    }


    size_t                size;
    size_t                start;
    std::array<size_t, N> extents;
    std::array<size_t, N> strides;
};

template<>
struct Matrix_slice<1> {
  public:
    size_t operator()(size_t i) const
    {
      return i;
    }
    
    size_t size;
    size_t start;
    size_t extents;
    size_t strides;
};

template<>
struct Matrix_slice<2>  {
  public:
    size_t operator()(size_t i, size_t j) const
    {
      return i * strides[0] + j;
    }

    size_t                size;
    size_t                start;
    std::array<size_t, 2> extents;
    std::array<size_t, 2> strides;
};

template <size_t N>
bool same_extents(const Matrix_slice<N> &a, const Matrix_slice<N> &b) {
  return a.extents == b.extents;
}





