#pragma once

#include <initializer_list>

namespace Matrix_impl {
  using std::size_t;
  template<typename T, size_t N>
  struct Matrix_init {
    using type = std::initializer_list<typename Matrix_init<T, N - 1>::type>;
  };

  template<typename T>
  struct Matrix_init<T, 1> {
    using type = std::initializer_list<T>;
  };

  template<typename T>
  struct Matrix_init<T, 0>;
}

template<typename T, std::size_t N>
using Matrix_initializer = typename Matrix_impl::Matrix_init<T, N>::type;
