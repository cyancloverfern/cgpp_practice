#pragma once

#include <numeric>

template<typename T, std::size_t N>
class Matrix_ref;

template<typename T>
T dot_product(Matrix_ref<T, 1> const& a, Matrix_ref<T, 1> const& b)
{
  return std::inner_product(a.begin(), a.end(), b.begin(), 0.0);
}