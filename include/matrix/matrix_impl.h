#pragma once

#include <array>
#include <concepts>
#include <numeric>

namespace Matrix_impl {
  using std::size_t;

  template<typename List>
  bool check_non_jagged(List const& list)
  {
    auto i = list.begin;
    for (auto j = i + 1; j != list.end(); ++j)
      if (i->size() != j->size())
        return false;
    return true;
  }

  template<size_t N, typename I, typename List>
    requires (N > 1)
  void add_extents(I& first, List const& list)
  {
    assert(check_non_jagged(list));
    *first = list.size();
    add_extents<N - 1>(++first, *list.begin());
  }

  template<size_t N, typename I, typename List>
    requires (N == 1)
  void add_extents(I& first, List const& list)
  {
    *first++ = list.size();
  }

  template<size_t N, typename List>
  std::array<size_t, N> derive_extents(List const& list)
  {
    std::array<size_t, N> rtn;
    auto f = rtn.begin();
    add_extents<N>(f, list);
    return rtn;
  }

  template<typename T, typename Vec>
  void add_list(T const* first, T const* last, Vec& vec)
  {
    vec.insert(vec.end(), first, last);
  }

  template<typename T, typename Vec>
  void add_list(std::initializer_list<T> const* first, std::initializer_list<T> const* last, Vec& vec)
  {
    for (; first != last; ++first)
      add_list(first->begin(), first->end(), vec);
  }

  template<typename T, typename Vec>
  void insert_flat(std::initializer_list<T> list, Vec& vec)
  {
    add_list(list.begin(), list.end(), vec);
  }

  template<typename... Args>
  concept Requesting_element = (std::convertible_to<Args, size_t>&& ...);

  struct slice {
      slice() :start(-1), length(-1), stride(1) { }
      explicit slice(size_t s) :start(s), length(-1), stride(1) { }
      slice(size_t s, size_t l, size_t n = 1) :start(s), length(l), stride(n) { }

      size_t operator()(size_t i) const { return start + i * stride; }

      static slice all;

      size_t start;
      size_t length;
      size_t stride;
  };

  template<typename... Args>
  concept Requesting_slice = 
    ((std::convertible_to<Args, size_t> || std::same_as<Args, slice>) && ...) 
    && ((std::same_as<Args, slice>) || ...);

}

