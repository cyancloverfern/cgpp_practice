#pragma once

#include "matrix_concepts.h"
#include "matrix_utility.h"
#include "matrix_impl.h"
#include "matrix_slice.h"
#include "matrix_initializer.h"

#include <vector>
#include <initializer_list>
#include <cassert>
#include <concepts>
#include <type_traits>


template<typename T, size_t N>
class Matrix_ref;

template<typename T, size_t N>
class Matrix {
  public:
    // static constexpr size_t order = N;
    using value_type = T;
    using iterator = std::vector<T>::iterator;
    using const_iterator = std::vector<T>::const_iterator;

    Matrix() = default;
    Matrix(Matrix const&) = default;
    Matrix& operator=(Matrix const&) = default;
    Matrix(Matrix&&) = default;
    Matrix& operator=(Matrix&&) = default;
    ~Matrix() = default;

    template<typename U>
    Matrix(Matrix_ref<U, N> const& x)
      :desc{x.desc}, elems{x.begin(), x.end()}
    {
      static_assert(std::is_convertible_v<U, T>, "Matrix constructor: incompatilble element types");
    }

    template<typename U>
    Matrix& operator=(Matrix_ref<U, N> const& x)
    {
      static_assert(std::is_convertible_v<U, T>, "Matrix =: incompatilble element types");

      desc = x.desc;
      elems.assign(x.begin(), x.end());
      return *this;
    }

    template<typename... Exts>
    explicit 
    Matrix(Exts... exts)
      : desc{exts...}, elems(desc.size)
    { }

    Matrix(Matrix_initializer<T, N> init)
    {
      desc.extents = Matrix_impl::derive_extents(init);
      elems.reserve(desc.size);
      Matrix_impl::insert_flat(init, elems);
      assert(elems.size() == desc.size);
    }

    Matrix& operator=(Matrix_initializer<T, N> init)
    {
      desc.extents = Matrix_impl::derive_extents(init);
      elems.reserve(desc.size);
      Matrix_impl::insert_flat(init, elems);
      assert(elems.size() == desc.size);
      return *this;
    }


    template<typename U>
    Matrix(std::initializer_list<U>) = delete;
    template<typename U>
    Matrix& operator=(std::initializer_list<U>) = delete;

    static constexpr size_t order() { return N; }
    size_t extent(size_t n) const { return N; }
    size_t size() const { return elems.size(); }
    Matrix_slice<N> const& descriptor() const { return desc; }

    T* data() { return elems.data(); }
    T* const data() const { return elems.data(); }

    template<typename... Args>
      requires Matrix_impl::Requesting_element<Args...>
    T& operator()(Args... args);

    template<typename... Args>
      requires Matrix_impl::Requesting_element<Args...>
    T const& operator()(Args... args) const;

    template<typename... Args>
      requires Matrix_impl::Requesting_slice<Args...>
    Matrix_ref<T, N>& operator()(Args... args);

    template<typename... Args>
      requires Matrix_impl::Requesting_slice<Args...>
    Matrix_ref<T const, N>& operator()(Args... args) const;

    Matrix_ref<T, N - 1> operator[](size_t i) { return row(i); }
    Matrix_ref<T const, N - 1> operator[](size_t i) const { return row(i); }

    Matrix_ref<T, N - 1> row(size_t i);
    // {
    //   assert(n < rows());
    //   Matrix_slice<N - 1> row;
    //   Matrix_impl::slice_dim<0>(n, desc, row);
    //   return {row, data()};
    // }

    Matrix_ref<T const, N - 1> row(size_t i) const;

    Matrix_ref<T, N - 1> col(size_t i);
    Matrix_ref<T const, N - 1> col(size_t i) const;

    template<typename F>
    Matrix& apply(F f)
    {
      for (auto& x : elems) f(x);
      return *this;
    }

    template<typename M, typename F>
    Matrix& apply(M const& m, F f)
    {
      assert(same_extents(desc, m.descriptor));
      for (auto i = begin(), j = m.begin(); i != end(); ++i, ++j)
        f(*i, *j);
      return *this;
    }

    Matrix& operator =(T const& value)
    {
      return apply([&](T& a){ a = value; });
    } 

    Matrix& operator+=(T const& value)
    {
      return apply([&](T& a){ a += value; });
    }

    Matrix& operator-=(T const& value)
    {
      return apply([&](T& a){ a -= value; });
    }

    Matrix& operator*=(T const& value)
    {
      return apply([&](T& a){ a *= value; });
    }

    Matrix& operator/=(T const& value)
    {
      return apply([&](T& a){ a /= value; });
    }

    Matrix& operator%=(T const& value)
    {
      return apply([&](T& a){ a %= value; });
    }


    template<Matrix_type M>
    Matrix& operator+=(M const& x)
    {
      static_assert(x.order() == N, "+=: mismatched Matrix dimensions");
      assert(same_extents(desc, m.descriptor()));

      return apply(x, [](T& a, M::value_type& b){ a += b; });
    }

    template<Matrix_type M>
    Matrix& operator-=(M const& x)
    {
      static_assert(x.order() == N, "+=: mismatched Matrix dimensions");
      assert(same_extents(desc, m.descriptor()));

      return apply(x, [](T& a, M::value_type& b){ a -= b; });
    }

    iterator begin() { return elems.begin(); }
    const_iterator begin() const { return elems.cbegin(); }
    iterator end() { return elems.end(); }
    const_iterator end() const { return elems.cend(); }    

  private:
    Matrix_slice<N> desc;
    std::vector<T> elems;
};
 

template<typename T, size_t N>
Matrix<T, N> operator+(Matrix<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res += value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator-(Matrix<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res -= value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator*(Matrix<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res *= value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator/(Matrix<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res /= value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator%(Matrix<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res %= value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator+(Matrix_ref<T, N> const& m, T const& value)
{
  Matrix<T, N> res = m;
  res += value;
  return res;
}

template<typename T, size_t N>
Matrix<T, N> operator+(Matrix<T, N> const& a, Matrix<T, N> const& b)
{
  Matrix<T, N> res = a;
  res += b;
  return res;
}

template<typename T1, typename T2, size_t N,
  typename RT = Matrix<std::common_type_t<typename T1::value_type, typename T2::value_type>, N>>
Matrix<RT, N> operator+(Matrix<T1, N> const& a, Matrix<T2, N> const& b)
{
  Matrix<RT, N> res = a;
  res += b;
  return res;
}

template<typename T>
Matrix<T, 2> operator*(Matrix<T, 1> const& u, Matrix<T, 1> const& v)
{
  const size_t n = u.extent(0);
  const size_t m = v.extent(0);
  Matrix<T, 2> res(n, m);
  for (size_t i = 0; i != n; ++i)
    for (size_t j = 0; j != m; ++j)
      res(i, j) = u[i] * v[j];
  return res;
}

template<typename T>
Matrix<T, 1> operator*(Matrix<T, 2> const& m, Matrix<T, 1> const& v)
{
  assert(m.extent(1) == v.extent(0));

  const size_t n = m.extent(0);
  Matrix<T, 1> res(n);
  for (size_t i =  0; i != n; ++i)
    for (size_t j = 0; j != n; ++j)
      res(i) += m(i, j) * v(j);
  return res;
}

template<typename T>
Matrix<T, 2> operator*(Matrix<T, 2> const& m1, Matrix<T, 2> const& m2)
{
  size_t const n = m1.extent(0);
  size_t const m = m1.extent(1);
  assert(m == m2.extent(0));

  size_t const p = m2.extent(1);
  Matrix<T, 2> res(n, p);
  for (size_t i = 0; i != n; ++i)
    for (size_t j = 0; j != m; ++j)
      res(i, j) = dot_product(m1[i], m2.col(j));
}

