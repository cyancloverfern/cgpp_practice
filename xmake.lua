set_project("CGPP_PRACTICE")

set_languages("c++20")

add_rules("mode.debug", "mode.release")

add_includedirs("include")

includes("volume/*")
includes("test/*")