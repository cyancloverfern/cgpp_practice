target("test.glfw")
  set_kind("binary")
  add_files("*.cpp", "src\\glad.c")
  add_includedirs("include")
  add_links("glfw3")
  