target("volume.04")
  add_rules("qt.widgetapp")
  add_files("*.cpp", "*.h")
  add_links("glfw3")
  if is_plat("mingw") then
    if is_host("windows") then
      add_ldflags("-mconsole")
    end
  end
