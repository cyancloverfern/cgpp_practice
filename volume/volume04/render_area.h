#pragma once

#include <QtWidgets>
#include <vector>
#include <functional>

QT_BEGIN_NAMESPACE
class QPaintEvent;
QT_END_NAMESPACE

class RenderArea : public QWidget {
  Q_OBJECT;

  public:
    RenderArea(QWidget *parent = nullptr);

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    auto get_this() { return this; }
  public:
    QPainter* painter;
    std::vector<std::function<void(void)>> *paint_tasks;
  
  protected:
    void paintEvent(QPaintEvent *event) override;

};

