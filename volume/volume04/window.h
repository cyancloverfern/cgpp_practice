#pragma once

#include "render_area.h"
#include "task.h"

#include <QtWidgets>

class Window : public QWidget {
  Q_OBJECT
  public:
    Window();

  public slots:
    // void operationChanged();
    void sliderChanged(int value);
  private:
    QHBoxLayout*  main_layout;
    QVBoxLayout*  control_layout;
    QSlider*      smooth_level;
    QLabel*       smooth_level_display;
    QLabel*       vector_count_display;
    RenderArea*   render_area;

  private:
    std::vector<curve_f>* curve_container;
    curve_in_use m_curve_in_use;
};