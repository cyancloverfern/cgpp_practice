#include "window.h"

#include <utility>
#include <iostream>

void caculate_cruve(std::vector<curve_f>*, size_t);

Window::Window()
  : main_layout           (new QHBoxLayout), 
    control_layout        (new QVBoxLayout),
    smooth_level          (new QSlider),
    smooth_level_display  (new QLabel),
    vector_count_display  (new QLabel),
    render_area           (new RenderArea)
{
  control_layout->addWidget(smooth_level);
  control_layout->addWidget(smooth_level_display);
  control_layout->addWidget(vector_count_display);
  main_layout   ->addLayout(control_layout);
  main_layout   ->addWidget(render_area);
  setLayout(main_layout);

  render_area->paint_tasks->push_back(draw_grid_line_fn
    {render_area, render_area->painter, 1000, 1000});

  curve_container = new std::vector<curve_f>;
  curve_container->push_back(curve_f{{0, 0, 0, 0}, {410, 710, 0, 0}, {710, 330, 0, 0}});
  caculate_cruve(curve_container, draw_2d_test_fn::curve_count_suppose - 1);

  smooth_level->setRange(0, draw_2d_test_fn::curve_count_suppose - 1);
  connect(smooth_level, &QSlider::valueChanged, this, &Window::sliderChanged);
  smooth_level->setValue(0);
  smooth_level->valueChanged(0);

  m_curve_in_use.curve_container = curve_container;
  m_curve_in_use.index = 0;

  render_area->paint_tasks->push_back(draw_2d_test_fn
    {std::addressof(m_curve_in_use), render_area, render_area->painter});
}

void Window::sliderChanged(int value)
{
  smooth_level_display->setNum(value);
  vector_count_display->setNum(static_cast<int>((*curve_container)[value].size()));
  m_curve_in_use.index = value;
  render_area->update();
}

void caculate_cruve(std::vector<curve_f>* curve, size_t curve_object)
{
  assert(curve->size() == 1);

  for (size_t i = 0; i != curve_object; ++i) {
    curve_f new_curve;
    curve_f const& last_curve_ref = (*curve)[i];
    for (int point_index = 0; point_index != last_curve_ref.size(); ++point_index) {
      point4<float> current_point = last_curve_ref[point_index % last_curve_ref.size()];
      point4<float> last_point = last_curve_ref[(last_curve_ref.size() + point_index - 1) % last_curve_ref.size()];
      point4<float> next_point = last_curve_ref[(last_curve_ref.size() + point_index + 1) % last_curve_ref.size()];
      new_curve.push_back({0.33f * last_point.x + 0.67f * current_point.x, 0.33f * last_point.y + 0.67f * current_point.y, 0, 0});
      new_curve.push_back(point4<float>{0.33f * next_point.x + 0.67f * current_point.x, 0.33f * next_point.y + 0.67f * current_point.y, 0, 0});
    }
    curve->push_back(std::move(new_curve));
    new_curve.clear();
    std::cout << "curve index: " << i << "\n";
  }
}








