#pragma once

#include <QtWidgets>

#include <cassert>
#include <vector>

struct draw_grid_line_fn {
  QWidget* device;
  QPainter* painter;
  size_t width, height;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(Qt::gray); 
    for (size_t i_horizontal = 0; i_horizontal != width; i_horizontal += 20)
      painter->drawLine(0, i_horizontal, 1000, i_horizontal); 
    for (size_t i_vertical = 0; i_vertical != height; i_vertical += 20)
      painter->drawLine(i_vertical, 0, i_vertical, 1000); 
    painter->end();
  }
};

template<typename T>
struct point4 {
  T x, y, z, h; 
};

using curve_f = std::vector<point4<float>>;

struct curve_in_use {
  std::vector<curve_f>* curve_container;
  int index;
};

struct draw_2d_test_fn {
  static constexpr size_t curve_count_suppose = 21;


  curve_in_use* curve_container;
  QWidget* device;
  QPainter* painter;

  void operator()(void)
  {
    assert(curve_container->curve_container->size() == curve_count_suppose);
    painter->begin(device);
    painter->setPen(Qt::red); 
    curve_f const& be_paint_curve = (*curve_container->curve_container)[curve_container->index];
    for (size_t i = 0; i!= be_paint_curve.size(); ++i) {
        auto [x1, y1, z1, h1] = be_paint_curve[i % be_paint_curve.size()];
        auto [x2, y2, z2, h2] = be_paint_curve[(i + 1) % be_paint_curve.size()];
        painter->drawLine(QPointF{qreal{x1}, qreal{y1}}, QPointF{qreal{x2}, qreal{y2}});
    }
    painter->end();
  }

};

