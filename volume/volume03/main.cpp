#include "render_area.h"

#include <QApplication>
#include <QtGui>

#include <iostream>
#include <tuple>
#include <utility>

struct draw_grid_line_fn {
  QWidget* device;
  QPainter* painter;
  size_t width, height;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(Qt::gray); 
    for (size_t i_horizontal = 0; i_horizontal != width; i_horizontal += 20)
      painter->drawLine(0, i_horizontal, 1000, i_horizontal); 
    for (size_t i_vertical = 0; i_vertical != height; i_vertical += 20)
      painter->drawLine(i_vertical, 0, i_vertical, 1000); 
    painter->end();
  }
};

// Why use tuple rather than a mannual sturct ?
// In convenient future, tuple biult in reflection will auto make a better struct 

using float4 = std::tuple<float, float, float, float>;

constexpr float4 cube[8] = {
  {-0.5, -0.5, 2.5, 0},
  {-0.5,  0.5, 2.5, 0},
  { 0.5,  0.5, 2.5, 0},
  { 0.5, -0.5, 2.5, 0},
  {-0.5, -0.5, 3.5, 0},
  {-0.5,  0.5, 3.5, 0},
  { 0.5,  0.5, 3.5, 0},
  { 0.5, -0.5, 3.5, 0},
};

using edge = std::pair<size_t, size_t>;

std::vector<edge> endpoints = {
  {0, 1}, {1, 2}, {2, 3},
  {3, 0}, {0, 4}, {1, 5},
  {2, 6}, {3, 7}, {4, 5},
  {5, 6}, {6, 7}, {7, 4},
};

std::vector<float4> picture_vertices(8);

struct ancient_renfder_fn {
  QWidget* device;
  QPainter* painter;
  std::vector<edge>* edge_ref;
  std::vector<float4>* picture_vertices_ref;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(QPen{Qt::red, 10});
    for (auto [p1, p2] : *edge_ref) {
      auto [x1, y1, z1, a1] = (*picture_vertices_ref)[p1];
      auto [x2, y2, z2, a2] = (*picture_vertices_ref)[p2];
      painter->drawLine(QPointF{qreal{x1}, qreal{y1}}, QPointF{qreal{x2}, qreal{y2}});
    }
    painter->end();
  }
};

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  {
    size_t index = 0;
    for (auto [x, y, z, a] : cube) {
      float projective_x = x / z;
      float projective_y = y / z;
      picture_vertices[index++] = {1000 * (1 - ((projective_x - -1) / 2)), 1000 * ((projective_y - -1) / 2), 1, 0};
    }
  }

  RenderArea window;
  window.paint_tasks->push_back(
    draw_grid_line_fn
      {window.get_this(), window.painter, 1000, 1000});
  window.paint_tasks->push_back(
    ancient_renfder_fn
      {window.get_this(), window.painter, std::addressof(endpoints), std::addressof(picture_vertices)});
  std::cout << window.paint_tasks->size() << "\n";
  window.show();
  return app.exec();
}