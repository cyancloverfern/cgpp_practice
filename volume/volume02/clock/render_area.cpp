#include "render_area.h"

RenderArea::RenderArea(QWidget *parent)
  : QWidget(parent), painter(new QPainter)
{
  paint_tasks = new std::vector<std::function<void(void)>>;
}

QSize RenderArea::minimumSizeHint() const
{
  return QSize(400, 400);
}

QSize RenderArea::sizeHint() const
{
  return QSize(1000, 1000);
}

void RenderArea::paintEvent(QPaintEvent *event)
{
  for (std::function<void(void)> const& paint_task : *paint_tasks) {
    paint_task();
  }
  
}

