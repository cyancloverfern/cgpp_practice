#include "render_area.h"
#include <QApplication>
#include <iostream>
#include <QtGui>

struct draw_grid_line_fn {
  QWidget* device;
  QPainter* painter;
  size_t width, height;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(Qt::gray); 
    for (size_t i_horizontal = 0; i_horizontal != width; i_horizontal += 20)
      painter->drawLine(0, i_horizontal, 1000, i_horizontal); 
    for (size_t i_vertical = 0; i_vertical != height; i_vertical += 20)
      painter->drawLine(i_vertical, 0, i_vertical, 1000); 
    painter->end();
  }
};

struct draw_clock_fn {
  static constexpr QPointF hour_hand[5] {
    {0,   0},
    {25,  15},
    {100, 0},
    {25,  -15},
    {0,   0},
  };

  static constexpr QPointF mintue_hand[5] {
    {0,   0},
    {25,  50},
    {0,   200},
    {-25, 50},
    {0,   0},
  };

  QWidget* device;
  QPainter* painter;

  void operator()(void)
  {
    painter->begin(device);
    painter->translate(500, 500);

    painter->setPen(QPen{Qt::red, 5});
    painter->setBrush(QColor{0xff, 0xff, 0x00, 0x5f});
    painter->drawEllipse(QPointF{0, 0}, 400, 400);

    painter->setPen(QPen{Qt::black, 3});
    painter->setBrush(Qt::black);
    painter->drawLine(0, 0, 0, -300);

    painter->drawPolygon(hour_hand, 5);
    painter->drawPolygon(mintue_hand, 4);
    painter->end();
  }
};

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  RenderArea window;
  window.paint_tasks->push_back(
    draw_grid_line_fn
      {window.get_this(), window.painter, 1000, 1000});
  window.paint_tasks->push_back(
    draw_clock_fn
      {window.get_this(), window.painter});
  std::cout << window.paint_tasks->size() << "\n";
  window.show();
  return app.exec();
}