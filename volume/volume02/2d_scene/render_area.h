#pragma once

#include <QtWidgets>
#include <vector>
#include <functional>

QT_BEGIN_NAMESPACE
class QPaintEvent;
QT_END_NAMESPACE

struct draw_horizontal_fn {
  QWidget* device;
  QPainter* painter;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(Qt::black); 
    painter->drawLine(0, 500, 600, 800); 
    painter->end();
  }
};


class RenderArea : public QWidget {
  Q_OBJECT;

  public:
    RenderArea(QWidget *parent = nullptr);

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    auto get_this() { return this; }
  public:
    QPainter* painter;
    std::vector<std::function<void(void)>> *paint_tasks;
    draw_horizontal_fn draw_horizontal;
  
  protected:
    void paintEvent(QPaintEvent *event) override;

};

