#include "render_area.h"

RenderArea::RenderArea(QWidget *parent)
  : QWidget(parent), painter(new QPainter), draw_horizontal()
{
  // QWidget::setAttribute(Qt::WA_PaintOutsidePaintEvent, true);
  paint_tasks = new std::vector<std::function<void(void)>> {
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::black); this->painter->drawLine(0, 0, 500, 050); this->painter->end(); },
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::red); this->painter->drawLine(0, 0, 500, 100); this->painter->end(); },
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::red); this->painter->drawLine(0, 0, 500, 200); this->painter->end(); },
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::red); this->painter->drawLine(0, 0, 500, 300); this->painter->end(); },
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::red); this->painter->drawLine(0, 0, 500, 400); this->painter->end(); },
    [this]() { this->painter->begin(get_this()); this->painter->setPen(Qt::red); this->painter->drawLine(0, 0, 500, 500); this->painter->end(); },
  };
}

QSize RenderArea::minimumSizeHint() const
{
  return QSize(400, 400);
}

QSize RenderArea::sizeHint() const
{
  return QSize(1000, 1000);
}

void RenderArea::paintEvent(QPaintEvent *event)
{
  for (std::function<void(void)> const& paint_task : *paint_tasks) {
    paint_task();
  }
}

