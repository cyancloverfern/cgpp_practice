#include "render_area.h"
#include <QApplication>
#include <iostream>

struct draw_grid_line_fn {
  QWidget* device;
  QPainter* painter;
  size_t width, height;
  void operator()(void)
  {
    painter->begin(device);
    painter->setPen(Qt::gray); 
    for (size_t i_horizontal = 0; i_horizontal != width; i_horizontal += 10)
      painter->drawLine(0, i_horizontal, 1000, i_horizontal); 
    for (size_t i_vertical = 0; i_vertical != height; i_vertical += 10)
      painter->drawLine(i_vertical, 0, i_vertical, 1000); 
    painter->end();
  }
};



int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  RenderArea window;
  draw_horizontal_fn draw_horizontal;
  draw_horizontal.device = window.get_this();
  draw_horizontal.painter = window.painter;
  window.paint_tasks->push_back(draw_horizontal);
  window.paint_tasks->push_back(draw_grid_line_fn{window.get_this(), window.painter, 1000, 1000});
  std::cout << window.paint_tasks->size() << "\n";
  window.show();
  return app.exec();
}